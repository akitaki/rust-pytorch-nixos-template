use anyhow::Result;
use tch::Device;
use tch::Tensor;

fn main() -> Result<()> {
    tch::maybe_init_cuda();

    let cpu = Device::Cpu;
    let cuda = Device::Cuda(0);

    let a = Tensor::of_slice2(&vec![vec![2.0, 0.0], vec![0.0, 2.0]]).to(cuda);
    let x = Tensor::of_slice2(&vec![vec![3.0], vec![5.0]]).to(cuda);
    let y = a.matmul(&x);
    println!(
        "{}\n{}\n{}",
        a.to(cpu).to_string(100)?,
        x.to(cpu).to_string(100)?,
        y.to(cpu).to_string(100)?
    );

    Ok(())
}
