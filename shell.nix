{
  pkgs ? import <nixpkgs> {
    # Rust overlay
    overlays = [
      (import (builtins.fetchTarball {
        url = https://github.com/oxalica/rust-overlay/archive/master.tar.gz;
      }))
    ];
    # For PyTorch, which contains CUDA
    config.allowUnfree = true;
  }
}:

let
  # Full, untouched binary distribution of libtorch
  libtorch-bin-full = pkgs.stdenv.mkDerivation {
    name = "libtorch-bin-full";
    version = "1.9.0";
    dontBuilt = true;
    dontConfigure = true;
    dontStrip = true;
    src = pkgs.fetchzip {
      url = "https://download.pytorch.org/libtorch/cu111/libtorch-cxx11-abi-shared-with-deps-1.9.0%2Bcu111.zip";
      sha256 = "11y5z3bm2aniiggva2c3qfvbfjszfa9kqaqwhbnhcj1nr4bvh6vm";
    };
    installPhase = "mkdir -p $out && cp -r * $out";
  };
in
  pkgs.mkShell {
    buildInputs = with pkgs; [
      libtorch-bin-full
      # access to nvidia driver
      linuxPackages.nvidia_x11
      # for rust -sys crates
      openssl pkg-config gcc
      # rust
      rust-analyzer
      (rust-bin.stable.latest.default.override {
        extensions = [ "rust-src" ];
      })
    ];
    shellHook = ''
      export LIBTORCH=${libtorch-bin-full}
      export LD_LIBRARY_PATH=${pkgs.linuxPackages.nvidia_x11}/lib:${libtorch-bin-full}/lib:${toString ./.}/libtorch/lib
      rm -f rust-analyzer && ln -s ${pkgs.rust-analyzer}/bin/rust-analyzer rust-analyzer
    '';
  }

